function [] = drawBoxes(info,colour)
    
    box = info(1).BoundingBox;
        
    %set the colour so that each robot is bordered by its colour.
    %Plot a square using the boundix box info
    plot([round(box(1)), round(box(1) + box(3)), round(box(1) + box(3)), round(box(1)), round(box(1))],[round(box(2)), round(box(2)), round(box(2) + box(4)), round(box(2) + box(4)), round(box(2))], 'Color', colour);
    end 
