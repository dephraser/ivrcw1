\documentclass[a4paper]{article}
\usepackage{listings}
\usepackage{mathtools}
\usepackage{graphicx}

\lstset{language=Matlab, breaklines=true}
\begin{document}

\title{Introduction to Vision and Robotics Assignment 1 Report}
\author{David Fraser-s0912336 \and James Hulme-s0901522}
\date{November 3, 2011}

\maketitle
\newpage

\section{Introduction}

%Reiterate the task

Our task for this assignment was to create a system that would take a sequence
of one hundred frames from a video and identify the different coloured robots 
within each frame. When the robots had been identified the system also had to 
identify which direction each of the robots were facing and trace the robots 
path through each of the frames.

\section{Methods}

At the high level our algorithm does this:

\begin{enumerate}
    \item Read in the images and normalise their RGB values.
    \item Generate a background from the sequence of images.
    \item Steps through all the images and detect the robots in each of them.
    \item Detects the direction based on the centre of the robots and the
    centre of the arrow.
    \item Draws the direction onto the robots, and outlines the robots as the 
    images are displayed when stepping through.
    \item At the end display the generated background and place the paths each 
    robot took on top of the background.
\end{enumerate}

\subsection{Reading in the Files and Normalising}
\label{sec:norm}

The images are read into a four dimensional Matlab array with the Cartesian 
position of the pixel, its intensity in the red, green and blue channels, and 
the index of the image in the sequence.  As they are read in the RGB values are
normalised. The code for loading images can be found in 
appendix~\ref{lst:getFiles}\footnote{Appendix~\ref{lst:getFilesNonNorm} is an 
algorithm used purely for reading in the images for use for displaying outputs}
The normalisation algorithm was based on the method in the notes this is only
an approximation of the actual normalised values.  The squared and square root 
parts of the equation were not included to keep the computation cost down and 
so the final normalisation method is as follows:
\[
    \bigg(\frac{r}{r + g + b},\frac{g}{r + g + b},\frac{b}{r + g + b}\bigg)
\]
The code for the normalisation algorithm can be found in 
appendix~\ref{lst:normRGB}.

\subsection{Generating the Background Based on the Sequence}

The normalised images are passed to the background generation algorithm
along with a step value. This step value is used to select which of the images
in the sequence are used in the background generation. For example we do not 
want to use the first couple of images as these are where the robots are mostly
stationary so the algorithm will see them as a part of the background rather 
than being separate from it which is what we want.  Step is an integer which 
ideally should be a factor of the number of images there are in the sequence.
You can use any step size from 1 to the number of images, but if isn't a factor
warning message could appear, and unexpected behaviour would be very possible. 
The function selects every n\textsuperscript{th} image where n is the step. 

To generate the background, we take every n\textsuperscript{th} image and
calculate the median for each pixel, so lets say taking every 
n\textsuperscript{th} left us with 20 images.  Effectively the algorithm
creates an image where each pixel is an array of 20 pixel values.  The median
of those 20 is then chosen to be the true value of the pixel.

This was the first technique we tried for background generation but with the 
first of the supplied test sets we had a problem with the fact that the blue 
robot did not move very much, which left a blue 'shadow' of the robot on the 
image

\begin{figure}[h]
    \caption{Blue shadow effect}
    \centering
        \includegraphics[width=4in]{ReportIms/backgroundBlueGhost.jpg}
\end{figure}

We adjusted the background generation algorithm so that after the median had
generated the background we calculated the mean of it.  This took the mean
of the red, green and blue colour channels and assigned that value to the pixel
This removed the blue 'shadow' from the background in the test set.  The
algorithm generated a flat colour background.  The code for this can be found
in appendix~\ref{lst:genBG}.

\begin{figure}[h]
    \caption{Image with background subtracted}
    \centering
    \includegraphics[width=4in]{ReportIms/backgroundSubtracted.jpg}
\end{figure}

\subsection{Robot Detection}
\label{sec:circleDect}

To locate the robots we subtract the generated background from the normalised
image.  This removes the majority of features apart from robots,  leaving only
some patches of noise, which we compensate for later.  We then make 3 passes
with \verb+graythresh+, a built in Matlab function that takes the image and
calculates the threshold required in a black white image to turn it into
a binary image.  We trick it by passing in different colour channel in each
pass.  Imitating a black and white image.  Then our binary image, is 1 for
red/green/blue (depending on what channel we passed) and 0 for everything else.

The binary images are then passed though \verb+regionprops+ which can detect
certain properties of binary images.  We use it to collect the areas, location
of the boundary boxes, and the centres of all of the sections where there is
colour.  We then sort these by area, this selects the largest area of colour in
the image as the robot.  It does this for each of the three channels .  Giving
us the information about the three colour robots: The centroid, the bounding
box of the robot (a rectangle drawn around the extremities of the robot) and
the area of the coloured section (robot).  The function that does this can be
found in appendix~\ref{lst:getProps}.

We use the information from the bounding box to draw the rectangle around the
robot indicating a detection.  The centroid is used to track the path the robot
takes through the sequence.  The area is only used find the robots.

One issue we ran into was that when enough of the robot went out of view the
thresholding selected a large area of background.  This is because we
recalculate the thresholding for each image.  We tried various methods to stop
this behaviour, such as keeping track of the average area of each robot through
the sequence, but this failed.  In the end we settled on if the boundary box
was larger than 200*200 then it wasn't the robot.

This assumption is fine for the data that was provided but we could run into
troubles if we have any close-up sequences of the robots moving as the robot
will quite probably be larger than 200*200;

\subsection{Detecting direction}

After calculating the properties of an image, we pass the image and these 
properties to the function that attempts to detect which direction the robot 
is pointing.  This is done for each robot individually.

To get an individual robot we cut it out of the image.  To do this we use the
information about the bounding box to effectively copy into a new image only
the indices in the original image which lie in the bounding box.  We then take
the inverse of this image, which gives us the triangle (along with some
background in the corners).

\begin{figure}[h]
    \caption{The outside edge of the robot is cut away so only the triangle is
    left. The blue area is taken into account when calculating the centre of
    the triangle}
    \centering
    \includegraphics[width=4in]{ReportIms/distplotradius.jpg}
\end{figure}

Again we use \verb+graythresh+ to calculate the threshold, and we then turn it
into a binary image with the triangle and background in white and the robot in
black.  This image is inverted and the centre found. This is the centre of the
robot.  The uninverted image is then trimmed to 90\% radius of the robot so
that the pixels we are dealing with are only part of the robot and are not part
of the background around it (the detected robots are not quite uniform).  Then
we are able to calculate the centre of the triangle we use the method as we did
when detecting the robots in section~\ref{sec:circleDect}.  The centre of the
triangle if detected properly is in line with apex of the triangle.  Thus
having the centre of the robot and the centre of the triangle, we can plot
a line starting at the centre of the robot and passing through the apex of the
triangle.  Which indicates direction.

\begin{figure}[h]
    \caption{Robots with their boundary boxes and direction vectors}
    \centering
    \includegraphics[width=4in]{ReportIms/perfect.jpg}
\end{figure}

We tried to find a way to make the line a standard length, but could not find
out how, so unfortunately the lines indicating direction will have their size
vary constantly.

This function can be found in appendix~\ref{lst:dectDirection}.

\subsection{Plotting the Robot's Path}

Plotting the paths is very simple once the robot's centre is known.  For each
image in the sequence, we recalculate the centre of each of the robots.  These
are then stored in arrays, one array for the \textit{x} coordinate, and one
array of \textit{y} coordinates for each of the robots.  Then at the end we can
just use Matlab's built in plot command to plot all the points for us, one line
for each robot.

The code for this is in appendix ~\ref{lst:getThisPartyStarted}.

\section{Results}

As well as the two data sets that were provided, we went and captured three
sets of images of our own, making sure they had different conditions to the
provided sets.  When we captured our own data we ended with pure green frames
appearing a few times in the sequences.  We believe these were from the camera
starting.  To overcome this we wrote a function to check if an image was
missing an entire colour channel.  The code for this is in
appendix~\ref{lst:isFlatCol}. In our case they were missing red and blue.  This
was causing \verb+regionprops+ to fail.  So if the image was missing a colour
channel we skipped it.

\subsection{Definition}
We have talked a lot so far about locating the robots and finding their
direction, but for a substantial analysis of our algorithms, these need to be
properly defined.

\begin{enumerate}
    \item Detecting a robot - A rectangle is drawn around what the algorithm
    detects as the robot.  If the robot is entirely contained in this rectangle
    then the robot can be declared detected.
    \item Tracking the robot - If the rectangle encloses the robot from frame
    to frame then the robot can be considered tracked this tracking is then
    plotted at the end as the path the robot took.
    \item Determining direction - This is trickier to define.  We are taking it
    that the arrow can be a bit to the side of the apex and that is still an
    accurate direction determination.
\end{enumerate}

\subsection{Test Set 1}

Test set one was the easiest of all the datasets we tested on.  It had very good
lighting, an unpatterned fairly uniform background.  It was with this dataset 
that we did almost all of the development of our algorithms.

Through the normalisation and background subtraction, we end up with very clear
images to work with, the three robots are very visible, there is some noise 
from the shadows but this is taken care of by the sorting via area to detect
the robots.

The first frame is a bit of an anomaly:  It is far far darker than the rest of
the frames.  This throws off of the detection at the start.  Only the red robot
is detected and it detected as the green robot.  This is what accounts for the
incorrect track.

With our direction detection, we found it was fairly good.  The robots go out
of focus for some periods of time, and this affects our accuracy.  And when the
robots start getting smaller as they move away from the camera the accuracy
suffers at times.  The worst detections are when the red and green robots
move off screen.  When the triangle has disappeared but the robot is still
detected the direction gets very confused and starts to point behind it.

Below are the results:

\vspace{1cm}
\resizebox{12cm}{!}{
    \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
        \hline
        Testset & Detect & & & Direction & & Track \\
        \hline
        & Correct & Missed & Invalid & Correct & Incorrect & Correct & Broken
        & Incorrect \\
        \hline
        Given 1 & 297 & 2 & 1 & 241 & 59 & 2 & 0 & 1 \\
        \hline
    \end{tabular}
}

\vspace{1cm}
As you can see we have exceptionally high accuracy for detecting the robots
the only thing stopping it from being 100\% is the first very dark frame.  It
is also this dark frame that is to blame for the incorrect track.  Because the
red robot is misdetected as the green robot, this led to a point in the green
track being far away from the rest of the path.  The direction accuracy is
fairly good, as we said before a lot of it is probably because of the focusing
issues.


 
\subsection{Test Set 2}

Test set two is made more difficult by the textured carpet.  In particular it
affected the detections of the green robot quite a lot.  We are not sure
exactly why this is, but have heard that webcams tend to get confused between
carpets and the colour green, but are not sure of the veracity of this claim.

Again this set was well lit.  There was no very dark frame at the start which
was very nice as meant we didn't start off with some misclassifications.

The direction detection struggled on this test set.  We believe this is because
of the size of the robots and the textured carpet confusing the triangle
detection.  The carpet also lead to there being more noise around than in the
first test set.  It is probably this noise that led to the green robot not
being detected for the last 15 or so frames.

Below are the results:

\vspace{1cm}
\resizebox{12cm}{!}{
    \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
        \hline
        Testset & Detect & & & Direction & & Track \\
        \hline
        & Correct & Missed & Invalid & Correct & Incorrect & Correct & Broken
        & Incorrect \\
        \hline
        Given 2 & 284 & 16 & 0 & 186 & 114 & 2 & 1 & 0 \\
        \hline
    \end{tabular}
}

\vspace{1cm}
As we said above fairly good on detecting robots until the green just stops
being recognised.  The direction detection suffers because of the noise from
the carpet.  Again there were two correct tracks, and this time one broken
track because of the green robot disappearing.

\subsection{Our Data 1}

The first of our data sets started to show some weaknesses in our
implementation.  This set was a lot darker than the ones we were provided with.
There were also lots more shadows around the robots.  And this showed in the 
increase of inaccuracy of robot detection.

At one point when the blue went off the screen our algorithms found it again
in the triangle in the red robot.  And it then popped up now and then in the
bottom right corner for the rest of the experiment.

The results are below:

\vspace{1cm}
\resizebox{12cm}{!}{
    \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
        \hline
        Testset & Detect & & & Direction & & Track \\
        \hline
        & Correct & Missed & Invalid & Correct & Incorrect & Correct & Broken
        & Incorrect \\
        \hline
        Our 1 & 264 & 24 & 12 & 253 & 47 & 2 & 0 & 1 \\
        \hline
    \end{tabular}
}

\vspace{1cm}
\subsection{Our Data 2}

The second data set we captured was also very dark and had lots of shadows.
And again we didn't have the greatest performance.

This time our direction detection went all over the place.  The darkness levels
and shadows will have been a real problem for it.  The green robot was also
being detected after it had disappeared.

\vspace{1cm}
\resizebox{12cm}{!}{
    \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
        \hline
        Testset & Detect& & & Direction & & Track \\
        \hline
        & Correct & Missed & Invalid & Correct & Incorrect & Correct & Broken
        & Incorrect \\
        \hline
        Our 2 & 242 & 43 & 15 & 177 & 123 & 2 & 0 & 1 \\
        \hline
    \end{tabular}
}

\vspace{1cm}
\subsection{Our Data 3}

Our third dataset is by far and away our worst.  The first time we tried with
our implementation none of the robots were detected.  At this stage we were
using 100*100 as our threshold for being too big to be a robot.  We realised
this was too small, so tried, 300, 250 and 200 and settled on 200.  This
improved the situation, we could now pick up red and green, but blue is still
never picked up.  The dark and shadows will be causing lots of noise that make
the blue robot always fall outside of the size threshold.  In addition to the 
dark and shadows we also put two dark strips either side of the testing area.
This will have affected the dectDirection algorithm as that uses black and
white to determine the size.


\begin{figure}[h]
    \caption{Traces showing red and green have been detected but blue has not}
    \centering
    \includegraphics[width=4in]{ReportIms/nobluepath.jpg}
\end{figure}

The results are below:

\vspace{1cm}
\resizebox{12cm}{!}{
    \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
        \hline
        Testset & Detect & & & Direction & & Track \\
        \hline
        & Correct & Missed & Invalid & Correct & Incorrect & Correct & Broken
        & Incorrect \\
        \hline
        Our 3 & 160 & 140 & 0 & 160 & 140 & 2 & 1 & 0 \\
        \hline
    \end{tabular}
}
\vspace{1cm}

\section{Discussion}

%What worked well
Our program was very good at detecting a suitable background image to help with
isolation of the robots.

We think the detection of the robots worked well, but especially the robot 
detection. It went though many iterations and factorisations to improve it.

%TODO: relate back to results
 
%Whats its limitations

We feel one of the limitations is that when we are deciding whether a robot is 
still on the stage or not, we use an arbitrary  value to compare against the 
size of the robot in order to decide if it is actually a robot or some 
colourisation within the background.

We also feel that the algorithm is very sensitive to the low light conditions.
it preforms well when there is a high contrast and both the robots and the 
background are well lit.

There are definitely improvements that we can make.

We need to develop a better method for checking if a robot has left the image.
Possibly using some statistical method.  It will be more computationally
expensive but we believe it will be worth it to help it cope with a wider range
of situations.

Along these lines we should also look into a way to decrease the dependency on 
the light level of the input images.  We could look into algorithms that
increase contrast levels.

We would also implement some smoothing into our functions.  This will help
reduce motion blur helping to increase the accuracy of the direction detection.

We would also try and capture our images a lossless format like .png.  It will
take more time computationally.  But the increase in quality is worth it as we
will be getting far more accurate results.

In conclusion we believe that we have designed and implemented a very effective
if slightly basic computer vision system.  Our results show it is effective
even if it does have its problems.  But it does work.

\section {References}

The only sources we consulted were the lectures notes for IVR and the official
Matlab documentation online.

\section{Mark Distribution}

We believe we both spent and equal amount of time and effort into this project
and so request that marks be distributed 50:50.


\newpage

\appendix
\section{getFiles.m}
\label{lst:getFiles}
\lstinputlisting{getFiles.m}

\section{getFilesNonNorm.m}
\label{lst:getFilesNonNorm}
\lstinputlisting{getFilesNonNorm.m}

\section{genBG.m}
\label{lst:genBG}
\lstinputlisting{genBG.m}

\section{getThisPartyStarted.m}
\label{lst:getThisPartyStarted}
\lstinputlisting{getThisPartyStarted.m}

\section{normRGB.m}
\label{lst:normRGB}
\lstinputlisting{normRGB.m}

\section{dectDirection.m}
\label{lst:dectDirection}
\lstinputlisting{dectDirection.m}

\section{drawBoxes.m}
\lstinputlisting{drawBoxes.m}

\section{getProps.m}
\label{lst:getProps}
\lstinputlisting{getProps.m}

\section{euclidDist.m}
\lstinputlisting{euclidDist.m}

\section{isFlatCol.m}
\label{lst:isFlatCol}
\lstinputlisting{isFlatCol.m}

\end{document}

