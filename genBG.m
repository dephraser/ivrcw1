function background = genBG(imageArr, step) 
% pass in the array of images and the desired stepsize, passing in stepsize to make it easier to experiment.
	
	[H,W,C,N] = size(imageArr);
	
	stepSize = step;
	
	maxIndex = N/stepSize;
	%sets up variables for loop
	
	slimImArr = zeros(H,W,C,maxIndex);
	%the array that the selected images go into
	
	%loop through full array selecting every nth image
	for i = 1:maxIndex
		slimImArr(:,:,:,i) = imageArr(:,:,:,(i*stepSize));
	end
	
	%use median to generate background, and normalise that medianed image
	bg = normRGB(median(slimImArr, 4));
	
	%sets every pixel to the mean of its 3 colour channels but changes it to 2D
	bg2D = mean(bg,3);
	%because bg2D is a 2D array so have to assign it to each colour channel of the background to keep it 3D so it will play nice with everything else.
	bg(:,:,1) = bg2D;
	bg(:,:,2) = bg2D;
	bg(:,:,3) = bg2D;
	
	background = bg;



