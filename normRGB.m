% takes one jpg image as argument.  return variable is normIm (normalised image)
function normIm = normRGB(loaded)
	loaded = double(loaded); % cast to a double
	[H,W,Z] = size(loaded); % Z is unused, but if just H,W then W is 3 times too big, because size returns a 3d array
	for i = 1:H,  
		for j = 1:W,
			% extract out the rgb values of a pixel
			R = loaded(i,j,1);
			G = loaded(i,j,2);
			B = loaded(i,j,3);
			
			% set the rgb values to the normalised versions
			loaded(i,j,1) = (R/(R+G+B));
			loaded(i,j,2) = (G/(R+G+B));
			loaded(i,j,3) = (B/(R+G+B));
		end
	end
	normIm = loaded; 
end
