function bobrocks = getThisPartyStarted(directory, step)
%pass in the directory where the images are. remember to include the trailing '/' step is the step size for generating
%the background.  bobrocks is just to have a return value, it doesn't really matter
	
	%the displays are just to give us humans an idea of what is going on
	disp('Reading Images...');
	%reads in the images into an array and normalises them all by rgb
	%Also creates an array of non normalised for displaying
	imageArray = getFiles(directory);
	imageArray2 = getFilesNonNorm(directory);
	disp('Generating Background...');
	
	%generates the background using the median method
	background = genBG(imageArray, step);
	
	disp('Killing all humans');
	
	%gets the N so that it works if there is a different amount of images sometimes
	[H,W,C,N] = size(imageArray);
	
	%Arrays to hold x and y coords of centroids to plot the path each robot took.
	redX = [];
	redY = [];
	greenX = [];
	greenY = [];
	blueX = [];
	blueY = [];
	
	for i = 1:N
	    %if a colour channel is missing skip the frame
	    flat = isFlatCol(imageArray(:,:,:,i));
	    if ~flat
		disp([num2str(i), ' of ', num2str(N)]);
		%subtracts background from current image, talked about method in lectures
		outImage = imsubtract(imageArray(:,:,:,i), background);
        
        %get the info for the three robots in the image
		infoArray = getProps(outImage);
		
        rBound = infoArray(1).BoundingBox;
        gBound = infoArray(2).BoundingBox;
        bBound = infoArray(3).BoundingBox;
        
        rCent = infoArray(1).Centroid;
        gCent = infoArray(2).Centroid;
        bCent = infoArray(3).Centroid;
		
		%imshow(outImage); %this is for displaying the normRGB values
		imshow(double(imageArray2(:,:,:,i))/255); %This is for displaying original image
		
		hold on; % hold allows stuff be drawn on the image
		
		if (rBound(3) < 200) & (rBound(4) < 200)
		    drawBoxes(infoArray(1),[1,0,0]); %Draw the squares around each bot
		
		    dectDirection(outImage, infoArray(1),1); %Draw the 'arrow'
		    
		    %track its movements, to be plotted at end
		    redX = [redX rCent(1)];
		    redY = [redY rCent(2)];
		end
		
		if (gBound(3) < 200) & (gBound(4) < 200)
		    drawBoxes(infoArray(2),[0,1,0]); %Draw the squares around each bot
		
		    dectDirection(outImage, infoArray(2),2); %Draw the 'arrow'
		    
		    greenX = [greenX gCent(1)];
		    greenY = [greenY gCent(2)];
		end
		
		if (bBound(3) < 200) & (bBound(4) < 200)
		    drawBoxes(infoArray(3),[0,0,1]); %Draw the squares around each bot
		
		    dectDirection(outImage, infoArray(3),3); %Draw the 'arrow'
		    
		    blueX = [blueX bCent(1)];
		    blueY = [blueY bCent(2)];
		end
		hold off;
		
        pause(1);
		%pause to let us analyse the results, more useful when we have object highlighting etc
		end
	end
		%turn hold back on so paths can be drawn
		imshow(background)
		hold on;
		%Draw the path each bot took
        plot(redX,redY,'Color',[1,0,0]);
        plot(greenX,greenY,'Color',[0,1,0]);
        plot(blueX,blueY,'Color',[0,0,1]);

		%turn hold off for good form.
		hold off;
	
	%just lets us know that program ran all the way through ok
	bobrocks = 1;

 
