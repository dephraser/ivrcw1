function distance = euclidDist(xOne, xTwo, yOne, yTwo)
    distance = sqrt((xOne-xTwo)^2 + (yOne-yTwo)^2);
    
%Calculates the euclidean distance.  Used for detecting the triangle.
