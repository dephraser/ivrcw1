function info = getProps(image3D)

    %Get threshholds and generate black and white based images for each colour channel
    %Tricks graythresh by passing one colour channel
    thresh = graythresh(image3D(:,:,1));
    rBin = im2bw(image3D(:,:,1), thresh);
    
    thresh = graythresh(image3D(:,:,2));
    gBin = im2bw(image3D(:,:,2), thresh);
    
    thresh = graythresh(image3D(:,:,3));
    bBin = im2bw(image3D(:,:,3), thresh);
    
    %Get the region info
    rInfo = regionprops(rBin,'Centroid','BoundingBox','Area');
    gInfo = regionprops(gBin,'Centroid','BoundingBox','Area');
    bInfo = regionprops(bBin,'Centroid','BoundingBox','Area');
    
    %Sorts the region information by area, and selects the largest area of each
    %colour channel, this will be the robot. (if there is a robot)
    rSort = rInfo(1);
    
    for i = 2:length(rInfo)
        temp = rInfo(i);
        if temp.Area > rSort.Area
            rSort = temp;
        end
    end
    
    gSort = gInfo(1);
    
    for i = 2:length(gInfo)
        temp = gInfo(i);
        if temp.Area > gSort.Area
            gSort = temp;
        end
    end
    
    bSort = bInfo(1);
    
    for i = 2:length(bInfo)
        temp = bInfo(i);
        if temp.Area > bSort.Area
            bSort = temp;
        end
    end
    
    %returns an array consisting of the region info for the three robots
    info = [rSort, gSort, bSort];
