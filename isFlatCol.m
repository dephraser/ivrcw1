function flat = isFlatCol(inImage);
%Used to check if a frame doesn't have any pixels with any of a colour, i.e. no red
%or a flat green image (like in the datasets we captured

    red = sum(sum(inImage(:,:,1)));
    green = sum(sum(inImage(:,:,2)));
    blue = sum(sum(inImage(:,:,3)));
    %calculate the sum of each colour channel, then calculate the sum of those
    %this give the total for an entire image, 3 totals, one for each channel
    
    flat = false;
    
    %If any of them are empty return true, so that the frame can be ignored
    if red == 0 | blue == 0 | green == 0
        flat = true;
    end
    

