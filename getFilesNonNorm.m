%gets all .jpg images in a directory and places them in an array.
%we only care about the name, so use imageArr(x).name.

%when giving the argument, must place a / at the end or it breaks
function imageArr = getFilesNonNorm(directory)
	ls = strcat(directory,'*.jpg');
	tempImArr = dir(ls);
	
	%4th dimension is the index
	imageArrTemp = zeros(480,640,3,length(tempImArr));

	% for loop goes through reading in all the images in the array
	for i = 1:length(tempImArr),
		imageArrTemp(:,:,:,i) = (imread(strcat(directory,tempImArr(i).name)));
	end
	
	imageArr = imageArrTemp;

