function  direction = dectDirection(inputImage,properties,i)
%Purpose is to detect the triangle

    %Calculate the bounding box of each robot from the input image
    %The box is used to work out with portion of the image to cut out for
    %Triangle detection
    
    %Get the bounding box of the robot, and it centroid in the 480*640 coord system
    if i == 1
	rBox = properties(1).BoundingBox;
	rBigCent = properties(1).Centroid;
	
	if floor(rBox(2) + rBox(4)) > 480
	    yRange = 480;
	else
	    yRange = floor(rBox(2) + rBox(4));
	end
	
	if floor(rBox(1) + rBox(3)) > 640
	    xRange = 640;
	else
	    xRange = floor(rBox(1) + rBox(3));
	end
	%Makes sure that we are not indexing out of bounds of the array
	
	%Cuts out the image by selecting the range of indexes from the image we want
	%And only take the robots colour channel, same for all robots
	rCirc = inputImage(ceil(rBox(2)):(yRange), ceil(rBox(1)):(xRange),1);
	
	%get the negative of the robot to be able to pickout the circle
	rTri = imcomplement(rCirc);
	
	%get the thresh of the triangle and create a binary image
	thresh = graythresh(rTri);
	rTriTh = im2bw(rTri,thresh);
	
    [H,W] = size(rTriTh);
	
	%Used to keep a running total of the coordinates where the triangle is
	%located
	triX = 0;
	triY = 0;
	count = 0;
	
	%Get the properties for the cut out circle
	rCircProps = regionprops(imcomplement(rTriTh),'Area','Centroid','BoundingBox');
	
	%Sort it so largest area is first
    rSort = rCircProps(1);
    
    %Find the largest area which is the robot and get its centroid, using the
    %cut out images coord space
    for i = 2:length(rCircProps)
        temp = rCircProps(i);
        if temp.Area > rSort.Area
            rSort = temp;
        end
    end
    
    %calculate the centroid in the small coord system of the robot    
    rCentroid = rSort.Centroid;
    
    
    %Loop through array, if the pixel is white and within the radius of the circle minus
    %some leeway then add it the running total
	for i = 1:H
	    for j = 1:W
	        if (rTriTh(i,j) == 1) & (euclidDist(j,rCentroid(1),i,rCentroid(2)) < euclidDist(rCentroid(1),0,rCentroid(2),round(H/2)) - 0.1*euclidDist(rCentroid(1),0,rCentroid(2),round(H/2)))
	            triX = triX + j;
	            triY = triY + i;
	            count = count + 1;
	        end
	    end
	end
	
	%centre of gravity of the triangle
	triX = triX/count;
	triY = triY/count;
	
	%plot the line on the image that was shown in getThisPartyStarted
	plot([rBigCent(1),rBigCent(1) + -7*(rCentroid(1)-triX)],[rBigCent(2),rBigCent(2) + -7*(rCentroid(2) - triY)],'Color', [1,0,0],'LineWidth',5);
	
	end
	
	
	%same as above but for green
	if i == 2

	gBox = properties(1).BoundingBox;
	gBigCent = properties(1).Centroid;
	
	if floor(gBox(2) + gBox(4)) > 480
	    yRange = 480;
	else
	    yRange = floor(gBox(2) + gBox(4));
	end
	
	if floor(gBox(1) + gBox(3)) > 640
	    xRange = 640;
	else
	    xRange = floor(gBox(1) + gBox(3));
	end
	
	gCirc = inputImage(ceil(gBox(2)):(yRange), ceil(gBox(1)):(xRange),2);
	
	gTri = imcomplement(gCirc);
	
	thresh = graythresh(gTri);
	gTriTh = im2bw(gTri,thresh);
	
	triX = 0;
	triY = 0;
	count = 0;
	
	[H,W] = size(gTriTh);
	
	gCircProps = regionprops(imcomplement(gTriTh),'Area','Centroid','BoundingBox');
		
    gSort = gCircProps(1);
    
    for i = 2:length(gCircProps)
        temp = gCircProps(i);
        if temp.Area > gSort.Area
            gSort = temp;
        end
    end
    
    gCentroid = gSort.Centroid;
    
	for i = 1:H
	    for j = 1:W
	        if (gTriTh(i,j) == 1) & (euclidDist(j,gCentroid(1),i,gCentroid(2)) < euclidDist(gCentroid(1),0,gCentroid(2),round(H/2)) - 0.1*euclidDist(gCentroid(1),0,gCentroid(2),round(H/2)))
	            triX = triX + j;
	            triY = triY + i;
	            count = count + 1;
	        end
	    end
	end
	
    triX = triX/count;
	triY = triY/count;
	
	plot([gBigCent(1),gBigCent(1) + -7*(gCentroid(1)-triX)],[gBigCent(2),gBigCent(2) + -7*(gCentroid(2) - triY)], 'Color',[0,1,0],'LineWidth',5);
	
	end
	
	%same as above but for blue
	if i == 3
	
	bBox = properties(1).BoundingBox;
	
	bBigCent = properties(1).Centroid;

	if floor(bBox(2) + bBox(4)) > 480
	    yRange = 480;
	else
	    yRange = floor(bBox(2) + bBox(4));
	end
	
	if floor(bBox(1) + bBox(3)) > 640
	    xRange = 640;
	else
	    xRange = floor(bBox(1) + bBox(3));
	end

	bCirc = inputImage(ceil(bBox(2)):(yRange), ceil(bBox(1)):(xRange),3);
	bTri = imcomplement(bCirc);
	
	thresh = graythresh(bTri);
	bTriTh = im2bw(bTri,thresh);

	triX = 0;
	triY = 0;
	count = 0;
	
	[H,W] = size(bTriTh);
	
	bCircProps = regionprops(imcomplement(bTriTh),'Area','Centroid','BoundingBox');
		
    bSort = bCircProps(1);
    
    for i = 2:length(bCircProps)
        temp = bCircProps(i);
        if temp.Area > bSort.Area
            bSort = temp;
        end
    end
    
    bCentroid = bSort.Centroid;
    
	for i = 1:H
	    for j = 1:W
	        if (bTriTh(i,j) == 1) & (euclidDist(j,bCentroid(1),i,bCentroid(2)) < euclidDist(bCentroid(1),0,bCentroid(2),round(H/2)) - 0.1*euclidDist(bCentroid(1),0,bCentroid(2),round(H/2)))
	            triX = triX + j;
	            triY = triY + i;
	            count = count + 1;
	        end
	    end
	end
	
	triX = triX/count;
	triY = triY/count;
	
	plot([bBigCent(1),bBigCent(1) + -7*(bCentroid(1)-triX)],[bBigCent(2),bBigCent(2) + -7*(bCentroid(2) - triY)], 'Color', [0,0,1],'LineWidth',5);
	end

    direction = 1;
end

